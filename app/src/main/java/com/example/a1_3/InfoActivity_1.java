package com.example.a1_3;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.ToggleButton;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class InfoActivity_1 extends AppCompatActivity {

    private CheckBox carCheckbox;
    private EditText ageOfTheCar;
    private Button  sendButton;
    private TextView    summaryText;
    private EditText ownerName;
    private EditText carAge;
    private ToggleButton fourwheel;
    private RadioGroup type;
    private RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_1);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ageOfTheCar = (EditText) findViewById(R.id.ageOfTheCar);
        carCheckbox = (CheckBox) findViewById(R.id.carCheckbox);
        carCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ageOfTheCar.setVisibility(VISIBLE);  //Jos auto on valittu, näytetään seuraava kentää
                } else {
                    ageOfTheCar.setVisibility(GONE);  //Jos auto ei ole valittu, piilotetaan kenttä
                }
            }
        });

        sendButton = (Button) findViewById(R.id.sendButton);
        summaryText = (TextView) findViewById(R.id.summaryText);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               sendInfo();
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.infoactivity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.advanced:
                advancedOptions();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


 public void advancedOptions() {

        fourwheel = (ToggleButton) findViewById(R.id.four_wheel_drive);
        fourwheel.setVisibility(VISIBLE);

        type = (RadioGroup) findViewById(R.id.type);
        type.setVisibility(VISIBLE);

    }

    public void sendInfo(){
        summaryText.setText("");

        ownerName = (EditText) findViewById(R.id.ownerName);
        String owner = ownerName.getText().toString();        //get owner name

        carAge = (EditText) findViewById(R.id.ageOfTheCar);
        String age = carAge.getText().toString();
        int carAge = Integer.parseInt(age);

        int selectedType = type.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedType);
        CharSequence selectedRadioButton = radioButton.getText();


        if (carAge <= 10 && carAge >= 1 )        // Car age ranged 1-10 is valid
        {
            summaryText.append("Tiedot ovat oikein\n");
            summaryText.append("Nimeesi: " + owner + "\n");
            summaryText.append("Auton ikä: " + age + "\n");
            summaryText.append("Auton tyyppi: " + selectedRadioButton + "\n");

            if (fourwheel.isChecked() == true)
                summaryText.append("Neliveto: kyllä \n");
            else
                summaryText.append("Neliveto: ei\n");
        }
        else {
            summaryText.append("Tiedot ovat väärin");
        }



    }
}